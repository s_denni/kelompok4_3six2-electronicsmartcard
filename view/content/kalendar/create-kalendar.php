<div class="col-sm-12 portlets">

  <div class="widget">
    <div class="widget-header transparent">
      <h2><strong>Add</strong> Kalendar</h2>
      <div class="additional-btn">
        <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
        <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
        <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
      </div>
    </div>
    <div class="widget-content padding">
      <div id="horizontal-form">
        <form class="form-horizontal" role="form" method="post" action="esc_crud_kalendar.php">

          <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Tanggal</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inputEmail3" placeholder="">
            </div>
          </div>

          <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Kegiatan</label>
            <div class="col-sm-10">
              <textarea class="form-control" name="Contactmessage" style="height: 140px; resize: none;"></textarea>
            </div>
          </div>

          <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Add</button>
          </div>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>
