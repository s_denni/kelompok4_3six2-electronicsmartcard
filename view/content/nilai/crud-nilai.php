


      <div class="page-heading">
              <h1><i class='fa fa-table'></i> Daftar Nilai Siswa/i</h1>
      </div>
            <!-- Page Heading End-->				<!-- Your awesome content goes here -->

      <div class="row">
        <div class="col-md-12">
          <div class="widget">
            <div class="widget-header transparent">
              <h2><strong>List</strong> Nilai</h2>
              <div class="additional-btn">
                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
              </div>
            </div>
            <div class="widget-content">
              <div class="data-table-toolbar">
                <div class="row">
                  <div class="col-md-4">
                    <form role="form">
                    <input type="text" class="form-control" placeholder="Search...">
                    </form>
                  </div>
                  <div class="col-md-8">
                    <div class="toolbar-btn-action">
                      <a href="esc_create_nilai.php" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add new</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="table-responsive">
                <table data-sortable class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>NRP</th>
                      <th>Nama</th>
                      <th>KUM</th>
                      <th>Semester</th>
                      <th>Action</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td><strong>1</strong></td>
                      <td>000000000</td>
                      <td>Andrian Shymphony</td>
                      <td>3.5</td>
                      <td>5</td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="delete" class="btn btn-default"><i class="fa fa-power-off"></i></a>
                          <a href="esc_edit_nilai.php" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td><strong>2</strong></td>
                      <td>000000000</td>
                      <td>Diamond Name</td>
                      <td>4</td>
                      <td>3</td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="delete" class="btn btn-default"><i class="fa fa-power-off"></i></a>
                          <a href="esc_edit_nilai.php" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td><strong>3</strong></td>
                      <td>000000000</td>
                      <td>Diamond Name</td>
                      <td>4</td>
                      <td>3</td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="delete" class="btn btn-default"><i class="fa fa-power-off"></i></a>
                          <a href="esc_edit_nilai.php" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td><strong>4</strong></td>
                      <td>000000000</td>
                      <td>Diamond Name</td>
                      <td>4</td>
                      <td>3</td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="delete" class="btn btn-default"><i class="fa fa-power-off"></i></a>
                          <a href="esc_edit_nilai.php" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td><strong>5</strong></td>
                      <td>000000000</td>
                      <td>Diamond Name</td>
                      <td>4</td>
                      <td>3</td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="delete" class="btn btn-default"><i class="fa fa-power-off"></i></a>
                          <a href="esc_edit_nilai.php" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td><strong>6</strong></td>
                      <td>000000000</td>
                      <td>Diamond Name</td>
                      <td>4</td>
                      <td>3</td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="delete" class="btn btn-default"><i class="fa fa-power-off"></i></a>
                          <a href="esc_edit_nilai.php" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-edit"></i></a>
                        </div>
                      </td>
                    </tr>


                  </tbody>
                </table>
              </div>

              <div class="data-table-toolbar">
                <ul class="pagination">
                  <li class="disabled"><a href="#">&laquo;</a></li>
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
