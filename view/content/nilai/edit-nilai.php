<div class="col-sm-12 portlets">

  <div class="widget">
    <div class="widget-header transparent">
      <h2><strong>Add</strong> Nilai</h2>
      <div class="additional-btn">
        <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
        <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
        <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
      </div>
    </div>
    <div class="widget-content padding">
      <div id="horizontal-form">
        <form class="form-horizontal" role="form" method="post" action="esc_crud_nilai.php">

          <div class="form-group">
          <label class="col-sm-2 control-label">Semester</label>
          <div class="col-sm-10">
            <select class="form-control">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option selected="true">5</option>
              <option>6</option>
              <option>7</option>
            </select>
          </div>
          </div>

          <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">NRP</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="inputEmail3" placeholder="0000000000">
            </div>
          </div>

          <div class="col-md-12">
						<div class="widget">
							<div class="widget-header transparent">
								<h2><strong>Sortable</strong> Table</h2>
								<div class="additional-btn">
									<a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
									<a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
									<a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
								</div>
							</div>
							<div class="widget-content">
								<div class="table-responsive">
									<table data-sortable class="table">
										<thead>
											<tr>
												<th>No</th>
												<th>Matakuliah</th>
												<th>Nilai</th>
											</tr>
										</thead>

										<tbody>
											<tr>
												<th>1</th>
                        <th>Kalkulus</th>
                        <th><input type="text" class="form-control" id="inputEmail3" placeholder="100"></th>
											</tr>
                      <tr>
												<th>2</th>
                        <th>Indonesia</th>
                        <th><input type="text" class="form-control" id="inputEmail3" placeholder="80"></th>
											</tr>
                      <tr>
												<th>3</th>
                        <th>Data</th>
                        <th><input type="text" class="form-control" id="inputEmail3" placeholder="95"></th>
											</tr>
                      <tr>
												<th>4</th>
                        <th>Nama Matkul</th>
                        <th><input type="text" class="form-control" id="inputEmail3" placeholder="75"></th>
											</tr>
                      <tr>
												<th>5</th>
                        <th>Nama Matkul</th>
                        <th><input type="text" class="form-control" id="inputEmail3" placeholder="85"></th>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

          <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Add</button>
          </div>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>
