


      <div class="page-heading">
              <h1><i class='fa fa-table'></i> Nilai Anda</h1>
      </div>
            <!-- Page Heading End-->				<!-- Your awesome content goes here -->

      <div class="row">
        <div class="col-md-12">
          <div class="widget">
            <div class="widget-header transparent">
              <h2><strong>Toolbar</strong> Nilai</h2>
              <div class="additional-btn">
                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
              </div>
            </div>


              <div class="table-responsive">
                <table data-sortable class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>Kode Matkul</th>
                      <th>Nama Matkul</th>
                      <th>Nilai</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td><strong>A001BA</strong></td>
                      <td>Kalkulus</td>
                      <td>B</td>
                    </tr>

                    <tr>
                      <td><strong>A001BA</strong></td>
                      <td>Kalkulus</td>
                      <td>B</td>
                    </tr>

                    <tr>
                      <td><strong>A001BA</strong></td>
                      <td>Kalkulus</td>
                      <td>B</td>
                    </tr>

                    <tr>
                      <td><strong>A001BA</strong></td>
                      <td>Kalkulus</td>
                      <td>B</td>
                    </tr>

                    <tr>
                      <td><strong>A001BA</strong></td>
                      <td>Kalkulus</td>
                      <td>B</td>
                    </tr>




                  </tbody>
                </table>
              </div>

              <div class="data-table-toolbar">
                <ul class="pagination">
                  <li class="disabled"><a href="#">&laquo;</a></li>
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
