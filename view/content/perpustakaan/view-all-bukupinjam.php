


      <div class="page-heading">
              <h1><i class='fa fa-table'></i> Daftar Peminjaman Buku</h1>
      </div>
            <!-- Page Heading End-->				<!-- Your awesome content goes here -->

      <div class="row">
        <div class="col-md-12">
          <div class="widget">
            <div class="widget-header transparent">
              <h2><strong>Toolbar</strong> Daftar peminjaman Buku</h2>
              <div class="additional-btn">
                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
              </div>
            </div>
            <div class="widget-content">
              <div class="data-table-toolbar">
                <div class="row">
                  <div class="col-md-4">
                    <form role="form">
                    <input type="text" class="form-control" placeholder="Search...">
                    </form>
                  </div>
                  <div class="col-md-4">
                    <h4>Total Buku di Pinjam : <strong>6</strong></h4>
                  </div>
                </div>
              </div>

              <div class="table-responsive">
                <table data-sortable class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Tanggal Peminjaman</th>
                      <th>Tanggal Pengembalian</th>
                      <th>Nama Buku</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>01-05-2017</td>
                      <td>14-05-2017</td>
                      <td>The Tales Of Zestiria</td>
                    </tr>

                    <tr>
                      <td>2</td>
                      <td>20-09-2017</td>
                      <td>30-10-2017</td>
                      <td>How To be Android Developer</td>
                    </tr>

                    <tr>
                      <td>3</td>
                      <td>00-00-0000</td>
                      <td>00-00-0000</td>
                      <td>"example books title"</td>
                    </tr>

                    <tr>
                      <td>4</td>
                      <td>00-00-0000</td>
                      <td>00-00-0000</td>
                      <td>"example books title"</td>
                    </tr>

                    <tr>
                      <td>5</td>
                      <td>00-00-0000</td>
                      <td>00-00-0000</td>
                      <td>"example books title"</td>
                    </tr>

                    <tr>
                      <td>6</td>
                      <td>00-00-0000</td>
                      <td>00-00-0000</td>
                      <td>"example books title"</td>
                    </tr>



                  </tbody>
                </table>
              </div>

              <div class="data-table-toolbar">
                <ul class="pagination">
                  <li class="disabled"><a href="#">&laquo;</a></li>
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
